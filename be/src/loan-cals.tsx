// SOURCE: https://github.com/cfpb/loan-calc

// calculate the raw monthly payment
// pass the total amount of the loan, the APR, and the length of the loan in months
const _paymentCalc = (loanAmt, loanRate, loanTerm): number => {

  if(loanAmt < 20000 || loanAmt > 800000) {
    throw new Error('Amount must be between 20000 and 800000.');
  }

  if(loanTerm < 24 || loanTerm > 96) {
    throw new Error('The term must be between 24 and 96 months.');
  }

  // monthly interest rate
  const monthlyRate = (loanRate/100)/12;

  // calculate the monthly payment
  // MonthlyPayment = Principal * ( MonthlyInterest / (1 - (1 + MonthlyInterest)^ -Months))
  return loanAmt * (monthlyRate / (1 - Math.pow(1 + monthlyRate, -(loanTerm))));
};

// round numbers
const _roundNum = (num): number => {
  return Math.round(num);
};

export type CalcOptions = {
  amount: number;
  rate: number;
  termMonths: number;
}

const _cleanOpts = (opts): CalcOptions => {
  // clean up US currency formatted strings
  if (isNaN(opts.amount)) {
    opts.amount = parseFloat(opts.amount.replace(/[^0-9.]+/g,''));
  }

  // throw errors for strings and unsupported numerical values
  if (typeof opts.amount === 'undefined' || isNaN(parseFloat(opts.amount)) || opts.amount < 0) {
    throw new Error('Please specify a loan amount as a positive number');
  }

  if (typeof opts.rate === 'undefined' || isNaN(parseFloat(opts.rate)) || opts.rate < 0) {
    throw new Error('Please specify a loan rate as a number');
  }

  if (typeof opts.termMonths === 'undefined' || isNaN(parseFloat(opts.termMonths)) || opts.termMonths < 0) {
    throw new Error('Please specify the length of the term as a positive number');
  }

  return {
    amount: opts.amount,
    rate: opts.rate,
    termMonths: opts.termMonths || 360
  };
};

// pass the amount of the loan, percentage rate, and length of the loan in months
export const paymentCalc = (opts: CalcOptions): { monthlyPayment: number } => {

  opts = _cleanOpts(opts);

  // calculate monthly payment
  const monthlyPayment = _paymentCalc(opts.amount, opts.rate, opts.termMonths);

  // round the payment to two decimal places
  return { monthlyPayment: _roundNum(monthlyPayment) };
};

export const totalInterest = (opts: CalcOptions): { totalPrice: number } => {

  opts = _cleanOpts(opts);

  // calculate the monthly payment
  const monthlyPayment = _paymentCalc(opts.amount, opts.rate, opts.termMonths);

  // subtract the original loan amount from the total amount paid to get the raw interest paid
  const rawInterest = (monthlyPayment * opts.termMonths) - opts.amount;

  // round the value to two decimal places
  return { totalPrice: _roundNum(rawInterest) };
};
