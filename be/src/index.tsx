import * as bodyParser from "body-parser";
import express from "express";
import { paymentCalc, totalInterest } from "./loan-cals";

try {
  const app = express();

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  const port: number = parseInt(process.env.PORT || "", 10) || 3000;
  app.listen(port);

  app.get("/api/calc/rate", (req, res) => {
    const { amount, rate, termMonths } = req.query;

    res.json(paymentCalc({
      amount: parseInt(amount as string, 10),
      rate: parseFloat(rate as string),
      termMonths: parseInt(termMonths as string, 10)
    }));
  });

  app.get("/api/calc/total", (req, res) => {
    const { amount, rate, termMonths } = req.query;

    res.json(totalInterest({
      amount: parseInt(amount as string, 10),
      rate: parseFloat(rate as string),
      termMonths: parseInt(termMonths as string, 10)
    }));
  });

  console.log(`Express application is up and running on port: ${port}`);
} catch (error) {
  console.error("server: " + error);
}
