# Backend for the Kalkulačka app

Tested on the Node 16 version.

## Start server
`npm run server`

The app run on http://localhost:3000.

## Lint
`npm run lint`

## Build
`npm run build`

## Deployment
For deployment, `.env` file must contain correct values specified in `example.env` file in the source code.

After that, you just run `npm run server` and that's it.
