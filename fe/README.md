# Kalkulačka

Tested on the Node 16 version, and the last Google Chrome/Firefox.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development

1) Create the new `.env` file from the `.env.example`.
2) And then type to console:
```
npm run serve
```

The app will be run on http://localhost:8080.

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## Tests

### Unit tests
`npm run test:unit`

### E2E Tests
`npm run test:e2e`

## Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
