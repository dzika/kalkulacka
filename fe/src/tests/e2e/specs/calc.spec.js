// https://docs.cypress.io/api/introduction/api.html

describe("Basic smoke tests", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("h1", "Kalkulačka");
  });

  it("Input correct data", () => {
    cy.visit("/");
    cy.contains("h1", "Kalkulačka");

    cy.get("[data-cy=loan-input]").click().clear().type("100000").blur();

    cy.wait(500);

    cy.contains("[data-cy=info_box-total_loan_price]", "7 345 Kč");
    cy.contains("[data-cy=info_box-monthly_payment]", "4 473 Kč");
    cy.contains("[data-cy=summary_box-monthly_payment]", "4 473 Kč");

    cy.get("[data-cy=period-input]").click().clear().type("50").blur();

    cy.wait(500);

    cy.contains("[data-cy=info_box-total_loan_price]", "15 348 Kč");
    cy.contains("[data-cy=info_box-monthly_payment]", "2 307 Kč");
    cy.contains("[data-cy=summary_box-monthly_payment]", "2 307 Kč");

    cy.get("[data-cy=insurance-on").click({ force: true });

    cy.wait(500);

    cy.contains("[data-cy=info_box-total_loan_price]", "20 348 Kč");
    cy.contains("[data-cy=info_box-monthly_payment]", "2 407 Kč");
    cy.contains("[data-cy=summary_box-monthly_payment]", "2 407 Kč");
  });
});
