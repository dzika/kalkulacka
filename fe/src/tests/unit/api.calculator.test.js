import { calcMonthlyPayment, calcTotalPrice } from "../../api/calculator";

const correctlyRequest = {
  amount: 50000,
  rate: 6.9,
  termMonths: 30,
};

describe('Test the "calculator" API', () => {
  it("should test that monthly payment calc is correctly", async () => {
    const result = await calcMonthlyPayment(correctlyRequest);

    expect(result).toEqual(1819);
  });

  it("should test that total loan calc is correctly", async () => {
    const result = await calcTotalPrice(correctlyRequest);

    expect(result).toBe(4580);
  });

  it("should test incorrectly loan amount (too small)", async () => {
    try {
      await calcMonthlyPayment({
        amount: 10000,
        rate: 6.9,
        termMonths: 30,
      });
    } catch (error) {
      expect(error).toHaveProperty("response.status");
      expect(error?.response?.status).toBe(500);
    }
  });

  it("should test incorrectly period (too big)", async () => {
    try {
      await calcMonthlyPayment({
        amount: 50000,
        rate: 6.9,
        termMonths: 230,
      });
    } catch (error) {
      expect(error).toHaveProperty("response.status");
      expect(error?.response?.status).toBe(500);
    }
  });
});
