import http from "../services/http";

export async function calcMonthlyPayment({ amount, rate, termMonths }) {
  const response = await http.get("/api/calc/rate", {
    params: {
      amount,
      rate,
      termMonths,
    },
  });

  return response.data?.monthlyPayment || 0;
}

export async function calcTotalPrice({ amount, rate, termMonths }) {
  const response = await http.get("/api/calc/total", {
    params: {
      amount,
      rate,
      termMonths,
    },
  });

  return response.data?.totalPrice || 0;
}
