import axios from "axios";

const opts = process.env.BACKEND_API_URL
  ? {
      baseURL: process.env.BACKEND_API_URL,
    }
  : {};

export default axios.create(opts);
