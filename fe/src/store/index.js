import Vue from "vue";
import Vuex from "vuex";
import { calcMonthlyPayment, calcTotalPrice } from "../api/calculator";

Vue.use(Vuex);

const SET_CALCULATOR_PARAMS = "setCalculatorParams";
const SET_CALCULATOR_TOTAL_PRICE = "setCalculatorTotalPrice";
const SET_CALCULATOR_LOADING_RESULT = "setCalculatorLoadingResult";
const SET_CALCULATOR_MONTHLY_PAYMENT = "setCalculatorMonthlyPayment";

export const CALCULATOR_CALC_TOTAL_PRICE = "calculatorCalcTotalPrice";
export const CALCULATOR_CALC_ALL = "calculatorCalcAll";

export default new Vuex.Store({
  state: {
    calculator: {
      params: {
        amount: 0,
        rate: 0,
        termMonths: 0,
      },
      totalPrice: 0,
      monthlyPayment: 0,
      loading: false,
    },
  },
  mutations: {
    [SET_CALCULATOR_PARAMS]: (state, params) => {
      state.calculator.params = params;
    },
    [SET_CALCULATOR_TOTAL_PRICE]: (state, totalPrice) => {
      state.calculator.totalPrice = totalPrice;
    },
    [SET_CALCULATOR_LOADING_RESULT]: (state, loading) => {
      state.calculator.loading = loading;
    },
    [SET_CALCULATOR_MONTHLY_PAYMENT]: (state, monthlyPayment) => {
      state.calculator.monthlyPayment = monthlyPayment;
    },
  },
  actions: {
    [CALCULATOR_CALC_ALL]: async ({ commit, dispatch, state }, params) => {
      try {
        commit(SET_CALCULATOR_LOADING_RESULT, true);

        const { insurance } = params;
        delete params.insurance;
        commit(SET_CALCULATOR_PARAMS, params);

        const monthlyPayment = await calcMonthlyPayment(
          state.calculator.params
        );

        commit(SET_CALCULATOR_MONTHLY_PAYMENT, monthlyPayment + insurance);

        await dispatch(CALCULATOR_CALC_TOTAL_PRICE);
      } catch (e) {
        console.error(e);
      } finally {
        commit(SET_CALCULATOR_LOADING_RESULT, false);
      }
    },
    [CALCULATOR_CALC_TOTAL_PRICE]: async ({ commit, state }) => {
      try {
        commit(SET_CALCULATOR_LOADING_RESULT, true);

        const totalPrice = await calcTotalPrice(state.calculator.params);

        commit(SET_CALCULATOR_TOTAL_PRICE, totalPrice);
      } catch (e) {
        console.error(e);
      } finally {
        commit(SET_CALCULATOR_LOADING_RESULT, false);
      }
    },
  },
});
