import Vue from "vue";
import VueRouter from "vue-router";
import CalculatorPage from "../views/CalculatorPage.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "CalculatorPage",
    component: CalculatorPage,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
